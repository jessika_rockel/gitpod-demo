FROM gitpod/workspace-base

RUN sudo apt-get update
RUN sudo apt-get install -y libicu-dev libncurses-dev libgmp-dev zlib1g-dev 

RUN sudo curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | BOOTSTRAP_HASKELL_NONINTERACTIVE=1 BOOTSTRAP_HASKELL_INSTALL_STACK=1 BOOTSTRAP_HASKELL_INSTALL_HLS=1 BOOTSTRAP_HASKELL_ADJUST_BASHRC=1 sh

ENV PATH="$HOME/.cabal/bin:$HOME/.ghcup/bin:$HOME/.ghcup/env:${PATH}"